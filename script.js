'use strict'

//Первая часть

let numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', 0)

let personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
    toggleVisibleMyDB() {
        this.privat = !this.privat
    },
    writeYourGenres() {
        for (let i = 0; i < 3; i++) {
            this.genres.append(prompt(`Ваш любимый жанр под номером ${i+1}`))
        }
        this.genres.forEach((genre, index) => {
            console.log(`Любимый жанр #${index+1} - это ${genre}`)
        })
    },
    showMyDB (privat) {
        if (!privat)
        console.log(personalMovieDB)
    }
}

for(let i = 0; i < 2; i++) {
    let lastFilm = ''
    let lastFilmRate = ''
    while(lastFilm == '' || lastFilm.length > 50 || lastFilmRate == '') {
        lastFilm = prompt('Один из последних просмотренных фильмов?')
        lastFilmRate = prompt('На сколько оцените его?')
    }
    personalMovieDB.movies[lastFilm] = lastFilmRate
}

if (personalMovieDB.count < 10){
    alert('Просмотрено довольно мало фильмов')
} else if (personalMovieDB.count < 30){
    alert('Вы классический зритель')
} else if (personalMovieDB.count > 30){
    alert('Вы киноман')
} else{
    alert('Произошла ошибка')
}
